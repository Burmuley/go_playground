// +build linux !darwin

package main

import (
	"fmt"
)

type fileAttr struct {
	attrs []string
}

func (p *fileAttr) cleanAttrs(filePath string) error {
	return cleanAttrs(filePath, p.attrs)
}

func (p *fileAttr) getAttrs(filePath string) ([]string, error) {
	var attrValuesList []string
	for _, attr := range p.attrs {
		a, err := getAttrValue(filePath, attr)

		if err != nil {
			return nil, fmt.Errorf("can't get attributes values: %v", err)
		}

		for _, i := range a {
			if len(string(i)) > 0 {
				attrValuesList = append(attrValuesList, string(i))
			}
		}
	}

	return attrValuesList, nil
}

func (p *fileAttr) hasWFAttrs(filePath string) bool {
	attrs, err := listFileAttrs(filePath)

	if err != nil {
		return false
	}

	return checkKnownAttrs(attrs, p.attrs)
}

func newFileAttr() *fileAttr {
	return &fileAttr{attrs: []string{"user.xdg.origin.url", "user.xdg.referrer.url"}}
}
