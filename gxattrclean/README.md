# GXATTRSCLEAN tool

This tool is intended to implement "WhereFrom" attributes list and clean-up.

Such attributes added by modern web browsers and command-line tools (wget, for instance) keeping information about the downloaded file source.
These attributes can affect privacy.

This is educational project. I used this "idea" to train myself with Go development.

Code is implemented using  "Strategy" pattern.

License: GPLv2
