// +build darwin linux

package main

import (
	"fmt"
	"github.com/pkg/xattr"
)

type platformFile interface {
	getAttrs(filePath string) ([]string, error)
	hasWFAttrs(filePath string) bool
	cleanAttrs(filePath string) error
}

// checkKnownAttrs checks if at least one file attribute was found in desired list currentATTRNames
func checkKnownAttrs(cAttrList []string, knownAttrs []string) bool {
	for _, i := range cAttrList {
		for _, k := range knownAttrs {
			if i == k {
				return true
			}
		}
	}

	return false
}

func listFileAttrs(filePath string) ([]string, error) {
	xattrs, err := xattr.List(filePath)

	if err != nil {
		return nil, fmt.Errorf("can't get file attributes: %v", err)
	}

	return []string(xattrs), nil
}

func getAttrValue(filePath, attrName string) ([]byte, error) {
	whereFrom, err := xattr.Get(filePath, attrName)

	if err != nil {
		return nil, fmt.Errorf("error reading file attributes: %v", err)
	}

	return whereFrom, nil
}

func cleanAttrs(filePath string, attrs []string) error {
	for _, attr := range attrs {
		err := xattr.Remove(filePath, attr)

		if err != nil {
			return err
		}
	}

	return nil
}
