// +build darwin linux

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

var (
	cleanWhereFromAttr  bool
	currentPlatformFile platformFile
)

func processFile(filePath string, fileInfo os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if currentPlatformFile.hasWFAttrs(filePath) {
		fmt.Printf("Name: %s\n", filePath)
		fmt.Println("WhereFrom data:")

		attrs, err := currentPlatformFile.getAttrs(filePath)

		if err != nil {
			return fmt.Errorf("error getting attributes: %v", err)
		}

		for _, attr := range attrs {
			fmt.Printf("\t%s\n", attr)
		}

		if cleanWhereFromAttr {
			fmt.Print("Clearing 'Where From' attributes...")
			if err := currentPlatformFile.cleanAttrs(filePath); err != nil {
				return err
			}

			fmt.Println("Done!")
		}

		fmt.Println("")
	}

	return nil

}

func main() {

	// Define and parse commandline args
	cleanFlag := flag.Bool("clean", false, "Clean 'Where from' attributes if found.")
	flag.Parse()

	rootDir := "."
	if len(flag.Args()) >= 1 {
		rootDir = flag.Args()[0]
	}

	currentPlatformFile = newFileAttr()

	// Set global variables and start directory scan
	cleanWhereFromAttr = *cleanFlag

	err := filepath.Walk(rootDir, processFile)

	if err != nil {
		fmt.Println("Error occured:", err)
	}
}
