// +build darwin !linux

package main

import (
	"fmt"
	"howett.net/plist"
)

type fileAttr struct {
	attrs []string
}

func (p *fileAttr) cleanAttrs(filePath string) error {
	return cleanAttrs(filePath, p.attrs)
}

func (p *fileAttr) getAttrs(filePath string) ([]string, error) {
	var attrValuesList []string
	for _, attr := range p.attrs {
		a, err := getAttrValue(filePath, attr)

		if err != nil {
			return nil, fmt.Errorf("can't get attributes values: %v", err)
		}

		pa, err := p.parseBPList(a)

		if err != nil {
			return nil, fmt.Errorf("error parsing bplist: %v", err)
		}

		for _, i := range pa {
			if len(i) > 0 {
				attrValuesList = append(attrValuesList, i)
			}
		}
	}

	return attrValuesList, nil
}

func (p *fileAttr) hasWFAttrs(filePath string) bool {
	attrs, err := listFileAttrs(filePath)

	if err != nil {
		return false
	}

	return checkKnownAttrs(attrs, p.attrs)
}

func (p *fileAttr) parseBPList(attr []byte) ([]string, error) {
	data := make([]string, 2)
	_, bplistErr := plist.Unmarshal(attr, &data)

	if bplistErr != nil {
		return nil, fmt.Errorf("binary plist decode error: %v", bplistErr)
	}

	return data, nil
}

func newFileAttr() *fileAttr {
	return &fileAttr{attrs: []string{"com.apple.metadata:kMDItemWhereFroms"}}
}
